<?php
abstract class Car {
	var $make;
	var $model;
	var $mfgYear;
	var $odometerReading;

	function __construct($make, $model, $mfgYear, $odometerReading)
	{
		$this->make = $make;
		$this->model = $model;
		$this->mfgYear = $mfgYear;
		$this->odometerReading = $odometerReading;
	}

	abstract function changeOil();
	abstract function tireRotation();
	abstract function wheelAlignment();

	public function getMake(){
		return $this->make;
	}

	public function setMake($make){
		$this->make = $make;
	}

	public function getModel(){
		return $this->model;
	}

	public function setModel($model){
		$this->model = $model;
	}

	public function getMfgYear(){
		return $this->mfgYear;
	}

	public function setMfgYear($mfgYear){
		$this->mfgYear = $mfgYear;
	}

	public function getOdometerReading(){
		return $this->odometerReading;
	}

	public function setOdometerReading($odometerReading){
		$this->odometerReading = $odometerReading;
	}
	
}

class electricCar extends Car
{
	
	function __construct($make, $model, $mfgYear, $odometerReading)
	{
		parent::__construct($make, $model, $mfgYear, $odometerReading);
	}

	function tireRotation(){
		return "Electric Car: tireRotation done";
	}

	function wheelAlignment(){
		return "Electric Car: wheel alignment done";
	}

	function replaceSparkPlug(){
		return "Electric Car: Spark Plug changed";
	}
    
    function changeOil(){
        throw new Exception("Oil change not required for Electric Car");
    }
}

class gasCar extends Car
{
	
	function __construct($make, $model, $mfgYear, $odometerReading)
	{
		super($make, $model, $mfgYear, $odometerReading);
	}

	function tireRotation(){
		return "Gas Car: tireRotation done";
	}

	function wheelAlignment(){
		return "Gas Car: wheel alignment done";
	}

	function changeOil(){
		return "Gas Car: oil change done";
	}
}

class dieselCar extends Car
{
	
	function __construct($make, $model, $mfgYear, $odometerReading)
	{
		super($make, $model, $mfgYear, $odometerReading);
	}

	function tireRotation(){
		return "Diesel Car: tireRotation done";
	}

	function wheelAlignment(){
		return "Diesel Car: wheel alignment done";
	}

	function changeOil(){
		return "Diesel Car: oil change done";
	}
}



    


?>
