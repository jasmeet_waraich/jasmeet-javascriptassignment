import java.util.ArrayList;
import java.util.Vector;

/*
 This is an abstract class that represents a FileEntity.
 A file entity can be a folder or a file. It has a property name,
 and a behaviour that is abstract and will be defined by its subclasses
*/
abstract class FileEntity
{
  // members common to File & Folder
    String name;    
    public FileEntity(){
        
    }
    
    //abstract method which will be defined by child classes
    abstract void Display();
} //end of class FILEENTITY


/*
  This object represents a folder in a file system which
  can have some child elements like another child folder or 
  a file. It is a sublass of FileEntity and defines the abstract methods
  in its super class
*/
class Folder extends FileEntity
{
  // a folder can have multiple FileEntity (folders or files)
    static ArrayList<FileEntity> subItems = new ArrayList<FileEntity>();
   
    public Folder(String folderName){
        this.name = folderName;
    }
    
    // adds new child items to the object
    public void Add(FileEntity file){
        subItems.add(file);
    }
    
    // displays all the sub items
    public void Display(){
        System.out.println(this.name);
        for(FileEntity item: subItems){
            System.out.println(item.name);
        }
    }

    // deletes the sub item object passed as parameter
    public void delete(FileEntity file){
        subItems.remove(subItems.indexOf(file));
    }
  
} // end of class FOLDER


/*
    This object represents a file in the file system.
    Its a subclass of FileEntity. 

*/
class File extends FileEntity
{
  // content of File    
    String content; 

    public File(String fileName){
        this.name = fileName; 
    }
    
    //overloaded constructor that takes name and content of file
    //as parameter
    public File(String fileName, String content) 
    { 
        this.name = fileName; 
        this.content = content; 
    } 
    
    //displys the content of the file
    public void Display(){
        System.out.println(this.content);
    }
    
} //end of class FILE


/*
    This object represents the File System and calls the different
    file entity objects methods.
*/
class FileSystem
{
    Vector<FileEntity> m_VecFileEntity;
    public static void main(String[] args){
        Folder Grandparent = new Folder("GrandParent");
        Folder Parent = new Folder("Parent");
        File ChildA = new File("ChildA");
        ChildA.content ="This is the content for Child A";
        File ChildB = new File("ChildB");
        Folder ChildC = new Folder("ChildC");
        File GrandChild = new File("GrandChild");
        Grandparent.Add(Parent);
        Parent.Add(ChildA);
        Parent.Add(ChildB);
        Parent.Add(ChildC);        
        ChildC.Add(GrandChild);
        Grandparent.Display();
        ChildA.Display();

    }
  
} // end of class FILESYSTEM