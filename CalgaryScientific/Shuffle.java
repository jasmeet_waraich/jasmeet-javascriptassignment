import java.util.Random;

/*
    this object represents a behavior that it generates 
    unique random numbers between any two given numbers.
*/
public class Shuffle{

    //this function will print random numbers betweeen integers a & b
    public static void randomize(int a, int b) {
		
        // Random number generator	
        Random rgen = new Random();  	
		
        //initializing an array with size eqaul to the
        //length of numbers b/w a and b
        int size = b-a+1;
		int[] array = new int[size];
 
        //populating array with numbers starting from a
		for(int i=0; i< size; i++){
			array[i] = a+i;
		}
 
        //re arranging the array elements by generating 
        //a random position
		for (int i=0; i<array.length; i++) {
            //getting a random number within the length of array
		    int randomPosition = rgen.nextInt(array.length);
            
            //swapping the values at current position to
            // the position generated randomly
		    int temp = array[i];
		    array[i] = array[randomPosition];
		    array[randomPosition] = temp;
		}
 
        //printing array elements
		for(int s: array)
            System.out.println(s);
 
		
	}

    public static void main(String[] args){
        //calling function randomize to print numbers b/w 1 to 10 randomly
        randomize(1, 5);
    }
}