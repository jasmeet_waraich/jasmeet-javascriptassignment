
import java.util.Vector;
/*
   An object of class card represents one of the 52 cards in a
   standard deck of playing cards.  Each card has a suit and
   a value.
*/

class Card {

    public final static int SPADES = 0,       // Codes for the 4 suits.
                            HEARTS = 1,
                            DIAMONDS = 2,
                            CLUBS = 3;
                            
    public final static int ACE = 1,          // Codes for the non-numeric cards.
                            JACK = 11,       
                            QUEEN = 12,       
                            KING = 13;
                            
    private final int suit;   // The suit of this card, one of the constants
                              //    SPADES, HEARTS, DIAMONDS, CLUBS.
                              
    private final int value;  // The value of this card, from 1 to 11.
                             
    public Card(int theValue, int theSuit) {
            //initialize the Card object
        value = theValue;
        suit = theSuit;
    }
        
    public int getSuit() {
            // Return the int that codes for this card's suit.
        return suit;
    }
    
    public int getValue() {
            // Return the int that codes for this card's value.
        return value;
    }
    
    public String getSuitAsString() {
            // Return a String representing the card's suit.
            // (If the card's suit is invalid, "??" is returned.)
        switch ( suit ) {
           case SPADES:   return "Spades";
           case HEARTS:   return "Hearts";
           case DIAMONDS: return "Diamonds";
           case CLUBS:    return "Clubs";
           default:       return "??";
        }
    }
    
    public String getValueAsString() {
            // Return a String representing the card's value.
            // If the card's value is invalid, "??" is returned.
        switch ( value ) {
           case 1:   return "Ace";
           case 2:   return "2";
           case 3:   return "3";
           case 4:   return "4";
           case 5:   return "5";
           case 6:   return "6";
           case 7:   return "7";
           case 8:   return "8";
           case 9:   return "9";
           case 10:  return "10";
           case 11:  return "Jack";
           case 12:  return "Queen";
           case 13:  return "King";
           default:  return "??";
        }
    }
    
    public @Override String toString() {
           // Return a String representation of this card, such as
           // "10 of Hearts" or "Queen of Spades".
        return getValueAsString() + " of " + getSuitAsString();
    }


} // end class Card

/* 
    An object of type Deck represents an ordinary deck of 52 playing cards.
    The deck can be shuffled, and cards can be dealt from the deck.
*/

class Deck {

    private Card[] deck;   // An array of 52 Cards, representing the deck.
    int cardsUsed = 0;   
    public Deck() {
           // Create an unshuffled deck of cards.
       deck = new Card[52];
       int cardCt = 0; // How many cards have been created so far.
       for ( int suit = 0; suit <= 3; suit++ ) {
          for ( int value = 1; value <= 13; value++ ) {
             deck[cardCt] = new Card(value,suit);
             cardCt++;
          }
       }
    }
    
    public void shuffle() {
          // Put all the used cards back into the deck, and shuffle it into
          // a random order.
        for ( int i = 51; i > 0; i-- ) {
            int rand = (int)(Math.random()*(i+1));
            Card temp = deck[i];
            deck[i] = deck[rand];
            deck[rand] = temp;
        }
    }
    
   public Card dealACard() {
          // Deals one card from the deck and returns it.
        if (cardsUsed == 52)
           shuffle();
        cardsUsed++;
        return deck[cardsUsed - 1];
    }

} // end class Deck

/*
    An object of type Hand represents a hand of cards. 
    */
class Hand {

   private Vector hand;   // The cards in the hand.
   
   public Hand() {
           // Create a Hand object that is initially empty.
      hand = new Vector();
   }
   
   public void addCard(Card c) {
         // Add the card c to the hand 
         hand.addElement(c);
   }
   
   public Card getCard(int position) {
          // Get the card from the hand in given position
      if (position >= 0)
         return (Card)hand.elementAt(position);
      else
         return null;
   }
      
} //end class Hand

/*
    The object of class CardsGame represents the actual game. This object
    setsup the game and plays it with two players with cards picked up
    randomly.
*/
public class CardsGame{
    //objects of class Hand and Deck
    Hand playerOneHand;
    Hand playerTwoHand;
    Deck deck;
    int winner;
    public CardsGame(){
        playerOneHand = new Hand();     //initializing the objects
        playerTwoHand = new Hand();
        deck = new Deck();        
    }
    
    //this method allocates two cards each to the players and decides who wins
    public void playGame(){
        deck.shuffle();
        playerOneHand.addCard(deck.dealACard());
        playerOneHand.addCard(deck.dealACard());
        playerTwoHand.addCard(deck.dealACard());
        playerTwoHand.addCard(deck.dealACard());
        
        System.out.println("\n\t\tGame of Cards");
        
        System.out.println("Player 1 Hands\n");
        System.out.println(playerOneHand.getCard(0).toString());
        System.out.println(playerOneHand.getCard(1).toString());
        
        System.out.println("\nPlayer 2 Hands\n");
        System.out.println(playerTwoHand.getCard(0).toString());
        System.out.println(playerTwoHand.getCard(1).toString());
        
        //Rank 1 check if player 1 has cards with same value
        if(playerOneHand.getCard(0).getValue() == playerOneHand.getCard(1).getValue()){
            winner = 1;
        }
        //Rank 1 check if player 2 has cards with same value
        if(playerTwoHand.getCard(0).getValue() == playerTwoHand.getCard(1).getValue()){
            if(winner != 1)
                //if player 1 doesn't have cards with same value then
                //player 2 is the winner
                winner = 2;
            else
                //if player 1 also has the cards with same value then its a tie
                //proceed to check which one has the highest value cards
                winner = 0;
        }
        
        //Rank2 check if Players have cards with same suit
        if(playerOneHand.getCard(0).getSuit() == playerOneHand.getCard(1).getSuit()){
            
            winner = 1;
        }
        if(playerTwoHand.getCard(0).getSuit() == playerTwoHand.getCard(1).getSuit()){
            if(winner != 1)
              //if player 1 doesn't have cards with same suit then
             //player 2 is the winner
                winner = 2;
            else
                //in case both have cards with same suit nobody is the winner and
                //proceed to check which one has highest value card
                winner = 0;
        } 
        
        //if Rank 1 & Rank 2 doesn't qualify. Check for the highest value card.
        if(winner == 0) {
        int playerOneCardTotal = playerOneHand.getCard(0).getValue() + playerOneHand.getCard(1).getValue();
        int playerTwoCardTotal = playerTwoHand.getCard(0).getValue() + playerTwoHand.getCard(1).getValue();
        
        if(playerOneCardTotal > playerTwoCardTotal){
            winner = 1;
        } else {
            if(playerOneCardTotal == playerTwoCardTotal)
                winner = 0;
            else
            winner = 2;
        }
        }
        switch(winner){
            case 1:
                System.out.println("Player One Wins !");
                break;
            case 2:
                System.out.println("Player Two Wins !");
                break;
            case 0:
                System.out.println("Its a Tie !!");
                break;
            default:
                System.out.println("There is an Error !");
                break;
        }
        
        
    } //end of playGame()
    
    
    public static void main(String[] args){
        CardsGame game = new CardsGame();
        game.playGame();
        
    }
    
}  //end of CardsGame


