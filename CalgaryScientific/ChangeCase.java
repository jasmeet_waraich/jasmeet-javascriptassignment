import java.lang.Character;

/*
 This objects represents a behaviour that it takes a string 
 as parameter and changes its case in a certain pattern that
 is all vowels in uppercase and all consonants in lower case
*/

class ChangeCase{  
    
    //method to change the case of the string
    public void changeCase(String str){
        for (char ch: str.toCharArray()) {
            //checks if the char in the string is a vowel of consonant
            if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')    {
                ch = Character.toLowerCase(ch);                
            } else {
                ch = Character.toUpperCase(ch);
            }
            System.out.print(ch);
        }
    }
    
    public static void main(String[] args){
        ChangeCase obj = new ChangeCase();
        //if no parameter is passed, the execution will stop
        if(args.length == 0){
            System.out.println("You have not provided any string for testing");
            System.exit(1);
        } else {
            for(int index=0; index<args.length; index++){
                obj.changeCase(args[index]);
                System.out.print(" ");
            }
        }
    }
} // end of class CHANGECASE