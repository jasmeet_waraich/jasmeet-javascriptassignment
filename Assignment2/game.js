
        /* boolean var resultBoolean
            0 -> Computer Wins
            1 -> You win
            2 -> Its a tie. no one wins
        */
        
        /* 
            function to start the game and prompt user to enter his choice
            and then generate a random number to create computer's choice.
            Also it displays the result after calling function whoWins() 
        */
        function startGame(){
            document.getElementById("result").innerHTML = '';

            // array of game options that user can select
            var gamePieces = new Array("Rock", "Paper", "Scissors", "Dynamite");

            //prompt user to enter any choice
            var userChoice = prompt("Please Enter your choice: \nRock --- Paper --- Scissors --- Dynamite");

            //check for valid user choice
            if(userChoice == '' || userChoice == null || gamePieces.indexOf(userChoice) == -1){
                document.getElementById("result").innerHTML = '';
                return;
            }

            //display user choice
            document.getElementById("result").innerHTML = "<h1>You entered : "+userChoice +"</h1>";

            //get computer choice based on a random number returned by getRandomGamePiece()
            var computerChoice;
            switch(getRandomGamePiece(gamePieces.length)) {
                    
                case 0:
                    computerChoice = "Rock";
                    break;
                case 1:
                     computerChoice = "Paper";
                    break; 
                case 2:
                    computerChoice ="Scissors";
                    break;
                case 3:
                    computerChoice ="Dynamite";
                    break;
                default :
                    computerChoice = "Invalid "
            }
             document.getElementById("result").innerHTML += "<h2>Computer's Choice : "+computerChoice+"</h2>";
             
             //holds the choices user/computer has selected for 2 secs before displaying the result
            waitTime = window.setTimeout( function(){
            if(whoWins(userChoice, computerChoice)==1){
               document.getElementById("result").innerHTML = "<img id='image-result' src='winner.jpg' >";
            } else {
                if(whoWins(userChoice, computerChoice) == 2){
                   document.getElementById("result").innerHTML = "<img id='image-result' src='its-a-tie.png' >";
                } else {
                   document.getElementById("result").innerHTML = "<img id='image-result' src='you-lose.jpg' >";
                }
            }
                 document.getElementById("button").innerHTML="Play Again !";
            },2000);    
           
                             
        }
        
        
       //generates a random number based upon length of array 
        function getRandomGamePiece(arrLength){
            var randomSelection;
            randomSelection = Math.floor((Math.random() * arrLength) + 0);
            return randomSelection;                       
        }
        
        //compares the choices and returns a number 0,1 or 2.
        //refer to the top to know about the numbers
        function whoWins(userChoice,computerChoice){
            var resultBoolean=0;
            if(userChoice == "Rock"){
                if(computerChoice == "Paper"){
                    resultBoolean = 0;
                    return resultBoolean;
                }
                if(computerChoice == "Scissors") {
                    resultBoolean = 1;
                    return resultBoolean;
                }
                if(computerChoice == "Dynamite"){
                    resultBoolean = 0;
                    return resultBoolean;
                }
                else {
                    resultBoolean = 2;
                    return resultBoolean;
                }
                
            }

            else if(userChoice == "Paper"){
            
                if(computerChoice == "Rock"){
                    resultBoolean = 1;
                    return resultBoolean;
                }
                if(computerChoice == "Scissors") {
                    resultBoolean = 0;
                    return resultBoolean;
                }
                if(computerChoice == "Dynamite"){
                    resultBoolean = 0;
                    return resultBoolean;
                }
                else {
                    resultBoolean = 2;
                    return resultBoolean;
                }
                
            }

             else if(userChoice == "Scissors"){
                if(computerChoice == "Rock"){
                    resultBoolean = 0;
                    return resultBoolean;
                }
                if(computerChoice == "Paper") {
                    resultBoolean = 1;
                    return resultBoolean;
                }
                if(computerChoice == "Dynamite"){
                    resultBoolean = 0;
                    return resultBoolean;
                }
                else {
                    resultBoolean = 2;
                    return resultBoolean;
                }
            }
                else if(userChoice == "Dynamite"){
                if(computerChoice == "Rock"){
                    resultBoolean = 1;
                    return resultBoolean;
                }
                if(computerChoice == "Paper") {
                    resultBoolean = 0;
                    return resultBoolean;
                }
                if(computerChoice == "Scissors"){
                    resultBoolean = 0;
                    return resultBoolean;
                }
                else {
                    resultBoolean = 2;
                    return resultBoolean;
                }

                
            }
        }
        

