 //global variable
         var txtTitle, txtAuthor, txtDate; 
         var outputString = "";
            
        
        // function to intialize default values on page load
        function init(){
                      
            txtTitle = document.getElementById("txtTitle");
            txtAuthor = document.getElementById("txtAuthor");
            txtDate = document.getElementById("txtDate");
            document.getElementById("output").value = "Check out book info goes here";
        }
        
        //performs validation on check out button
        function validate() {
            
            //check for input Title if its empty or length of characters is more than 70
            if (txtTitle.value == "") {
                //display my error message
                alert("Please enter a Title");
                txtTitle.focus();
                return;

            } else if(txtTitle.value.length > 70){
                alert("Title cannot be more than 70 characters.\nPlease enter again");
                txtTitle.value = "";
                txtTitle.focus();
                return;
            }
            
            //check for input Author if it empty
            if (txtAuthor.value == "") {
                alert("please enter an Author Name");
                txtAuthor.focus();
                return;
            }
            
            //check for input Date if it empty or in valid format which is ##-##-####
            if (txtDate.value == "") {
                alert("please enter a Return Date");
                 txtDate.focus();
                return;
            } else if(!checkDate(txtDate.value)){
                alert("Please enter a valid date in correct format.\nFormat ##-##-####\nDate must be a future date");
                txtDate.value="";
                txtDate.focus();
                return;
            }
            
            //check for radio button if anyone of those is selected or not
            if(getCheckedRadioValue() == ''){
                alert("Please select a book type");
                document.getElementById("btnFiction").focus();
                return;
            }
            
            //final output string
            outputString += "Author: " + txtAuthor.value + " Title: "+ txtTitle.value + " Return Date: " + txtDate.value + " Book Type: "+getCheckedRadioValue()+"\n";
            
            document.getElementById("output").value = outputString;
           
            //call to reset() after setting the values to textarea
            reset();
            
        }
         
        /*checks the pattern of the date entered as of ##-##-####
            takes a date as a parameter
            returns a boolean
            true -> valid pattern
            false -> invalid pattern
        */
         function checkDate(dt){
            var dateRegex = /(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\d\d/
            // test the email against our emailRegex format.
            var matched = dateRegex.test(dt);
            return matched;
        }
        
        /*
            returns the checked radio button value
        */
        function getCheckedRadioValue()
            {
                //create a var to hold the select value (if any)
                var selectedBookType = "";
                //create another var to hold our array of radio btns
                var radioArray = document.getElementsByName("btnArray1");
                //loop through the array and find the checked value
                for(var index=0;index<radioArray.length;index++)
                {
                    // there should be only 1 checked button.
                    // When we find it, set our selectGender var
                    // to the value for that button
                    if(radioArray[index].checked)
                    {
                        selectedBookType = radioArray[index].value;
                    }
                }
                // this function needs to return a value for our
                // validation.  Returning it below.
                return selectedBookType;  
            }
        
        /*
        resets the values in all input field after validations have passed
        */
        function reset() {
            document.getElementById("txtTitle").value = "";
            document.getElementById("txtAuthor").value = "";
            document.getElementById("txtDate").value = "";
            var radioArray = document.getElementsByName("btnArray1");
            
            //iterates the radio button array and resets the checked radio button
            for(var index=0;index<radioArray.length;index++) {
                radioArray[index].checked = false;
            }
        }

