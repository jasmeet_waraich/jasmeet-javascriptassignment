/**
 * 
 */

function loadProvinces(){
	provArray = new Array("Ontario","Quebec","Nova Scotia","New Brunswick", "Manitoba","British Columbia", "Prince Edward Island",
						   "Saskatchewan","Alberta","Newfoundland and Labrador");
	provArray.sort();
	createOptions =  "";
	createOptions += "<option value=''>-Select-</option>" 
	for(index=0;index<provArray.length;index++){		
		createOptions += "<option value='"+provArray[index]+"'>"+provArray[index]+"</option>";				
	}
	document.getElementById("cboProv").innerHTML=createOptions;
}

function validateForm(){
	
	var name=document.getElementById("txtName");
	var email=document.getElementById("txtEmail");
	var city=document.getElementById("cboProv");
	if(name.value == ""){
		alert("Name can't be left blank");
		name.focus();
	} else if(email.value == ""){
		alert("Email can't be left blank");
		email.focus();
	} else if(city.value == ""){
		alert("City wasn't selected");
		city.focus();
	} else{
		alert("Mission Completed !")
	}
}
		
