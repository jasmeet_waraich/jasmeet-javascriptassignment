angular.module("", [])
 .controller('provinceCtrl', function($scope) {
   $scope.availableOptions = {  [
      {id: '0', name: '-Select-'},  
      {id: '1', name: 'Ontario'},
      {id: '2', name: 'Quebec'},
      {id: '3', name: 'Nova Scotia'},
      {id: '4', name: 'New Brunswick'},
      {id: '5', name: 'Manitoba'},
      {id: '6', name: 'British Columbia'},
      {id: '7', name: 'Prince Edward Island'},
      {id: '8', name: 'Saskatchewan'},
      {id: '9', name: 'Alberta'},
      {id: '10', name: 'Newfoundland and Labrador'}
    ],
    selectedOption: {id: '0', name: '-Select-'} //This sets the default value of the select in the ui
    };
});